# Passman Svelte

[![Latest Release](https://gitlab.com/binsky08/passman-svelte/-/badges/release.svg?order_by=release_at)](https://gitlab.com/binsky08/passman-svelte/-/releases)
[![Pipeline Status](https://gitlab.com/binsky08/passman-svelte/badges/main/pipeline.svg)](https://gitlab.com/binsky08/passman-svelte/-/pipelines)
[![Quality Gate Status](https://sonarqube.binsky.org/api/project_badges/measure?project=Passman-Svelte&metric=alert_status&token=sqb_408d46f5d727710f059fb51ce1895c205090d05a)](https://sonarqube.binsky.org/dashboard?id=Passman-Svelte)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

## About the project

$`\textcolor{red}{\text{-> This project is still in an unstable development (alpha) state - do not use in production!!! <-}}`$

**Passman Svelte** is a standalone Passman web client as modern alternative to the official Passman UI (integrated
in its Nextcloud app).

It's based on the modern, open-source [Svelte](https://svelte.dev/) JS framework, together
with [TypeScript](https://www.typescriptlang.org/) and [Vite](https://vitejs.dev/).

## Usage

This application requires the official [Nextcloud Passman App](https://apps.nextcloud.com/apps/passman) to be installed
in your Nextcloud.

The current state of development will be automatically published to an
own [GitLab page](https://binsky08.gitlab.io/passman-svelte/) as a public test instance.

No data is transferred to sources other than the specified Nextcloud server and of course your local browser.

Feel free to test the application
and [give feedback or report bugs](https://gitlab.com/binsky08/passman-svelte/-/issues).
Please do not report missing features, while the project is in early development state.

### Installation
- Requires an already running webserver like nginx or apache2 to deliver static files
- Create a webserver configuration for a custom subdomain like 'mypassman.example.com'
- Download passman-svelte-vX.X.X.zip of the latest release from https://gitlab.com/binsky08/passman-svelte/-/releases
- Extract the archive to the destination you want. It's recommended to use a custom webroot directory like `/var/www/passman-svelte`.

Or just run all commands at once:
```
curl -L -o /tmp/passman-svelte.zip https://gitlab.com/api/v4/projects/47240046/packages/generic/passman-svelte/latest/passman-svelte.zip
unzip /tmp/passman-svelte.zip -d /var/www/passman-svelte
rm /tmp/passman-svelte.zip
chown -R www-data:www-data /var/www/passman-svelte
```

## Development & Contribution

Please always sign your commits if you have a gpg signing key set up.

If you already have a gpg key, you can enable automatic signing for this repository by executing: `git config commit.gpgsign true`

Once you've cloned a project and installed dependencies with `npm install` (add `--force` if required), start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

### Recommended IDE Setup
[WebStorm](https://www.jetbrains.com/webstorm/) + [Svelte](https://plugins.jetbrains.com/plugin/12375-svelte)

or

[VS Code](https://code.visualstudio.com/) + [Svelte](https://marketplace.visualstudio.com/items?itemName=svelte.svelte-vscode)

## Building

To create a production version of this app:

```bash
npm run build
```
