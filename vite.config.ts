import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'
import packageJson from './package.json';

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [svelte()],
    define: {
        'import.meta.env.PACKAGE_VERSION': JSON.stringify(packageJson.version)
    }
})
