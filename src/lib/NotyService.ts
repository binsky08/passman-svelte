import { toast } from '@zerodevx/svelte-toast';
import type { IFormFieldError } from "@binsky/passman-client-ts/lib/Exception/FormFieldError";

export const notyInfo = (msg: string) => toast.push(msg);

export const notySuccess = (msg: string) =>
    toast.push(msg, {
        theme: {
            '--toastBackground': '#8BC34A',
            '--toastBarBackground': '#689F38'
        }
    });

export const notyWarning = (msg: string) =>
    toast.push(msg, {
        theme: {
            '--toastBackground': '#ffdb5b',
            '--toastBarBackground': '#edc242'
        }
    });

export const notyError = (msg: string) =>
    toast.push(msg, {
        theme: {
            '--toastBackground': '#F44336',
            '--toastBarBackground': '#D32F2F'
        }
    });

export const notyFormFieldErrors = (errors: IFormFieldError[]) => {
    errors.forEach((error: IFormFieldError) => notyError(error.error));
};
