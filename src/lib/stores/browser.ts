import { EncryptStorage } from 'encrypt-storage';

export enum BrowserStorageType {
    MEMORY = 'memory',
    SESSION = 'sessionStorage',
    PERSISTENT = 'localStorage'
}

/**
 * Custom encrypted browser storage handler that uses sessionStorage by default.
 * The get, set, del methods automatically use the storage type stored in the localStorage, or SESSION by default.
 */
export class BrowserStorage {
    private static defaultStorageType = localStorage.getItem("storage");
    private static instances: BrowserStorage[] = [];

    private encryptStorage: EncryptStorage;
    private fakeMemoryStorage = {};

    private constructor(private identifier: string, private storageType: BrowserStorageType) {
        this.encryptStorage = new EncryptStorage(import.meta.env.VITE_STORAGE_ENC_KEY ?? 'd3fau17k37', {
            prefix: '@' + identifier,
            storageType: storageType === BrowserStorageType.PERSISTENT ? BrowserStorageType.PERSISTENT : BrowserStorageType.SESSION
        });
        BrowserStorage.instances.push(this);
    }

    /**
     * Returns an existing BrowserStorage instance or creates a new one based on a unique identifier with the given storage type.
     */
    public static getInstance(identifier: string = "default", storageType: BrowserStorageType = undefined): BrowserStorage {
        for (let i = 0; i < BrowserStorage.instances.length; i++) {
            if (BrowserStorage.instances[i].identifier === identifier) {
                return BrowserStorage.instances[i];
            }
        }

        return new BrowserStorage(identifier, storageType ?? (BrowserStorage.defaultStorageType as BrowserStorageType));
    }

    /**
     * Check if there is at least one instance with the specified identifier, ignoring the BrowserStorageType.
     * @param identifier
     */
    public static hasInstance(identifier: string): boolean {
        for (let i = 0; i < BrowserStorage.instances.length; i++) {
            if (BrowserStorage.instances[i].identifier === identifier) {
                return true;
            }
        }
        return false;
    }

    public static getDefaultStorageType(): string {
        return BrowserStorage.defaultStorageType;
    }

    public static setDefaultStorageType(storageType: BrowserStorageType) {
        BrowserStorage.defaultStorageType = storageType;
        localStorage.setItem("storage", storageType);
    }

    public static clearAllStorageTypesForVaultGUID(vaultGUID: string) {
        // delete all vault browser storage instances, if exists
        BrowserStorage.getInstance('vault-' + vaultGUID, BrowserStorageType.MEMORY).clearDeleteStorage();
        BrowserStorage.getInstance('vault-' + vaultGUID, BrowserStorageType.SESSION).clearDeleteStorage();
        BrowserStorage.getInstance('vault-' + vaultGUID, BrowserStorageType.PERSISTENT).clearDeleteStorage();
    }

    public static ensureDeleteKeyForAllStorageTypes(storageIdentifier: string, keyToDelete: string) {
        BrowserStorage.getInstance(storageIdentifier, BrowserStorageType.MEMORY).del(keyToDelete);
        BrowserStorage.getInstance(storageIdentifier, BrowserStorageType.SESSION).del(keyToDelete);
        BrowserStorage.getInstance(storageIdentifier, BrowserStorageType.PERSISTENT).del(keyToDelete);
    }

    public updateStorageType(storageType: BrowserStorageType) {
        if (this.storageType !== storageType) {
            this.storageType = storageType;
            if (storageType !== BrowserStorageType.MEMORY) {
                this.encryptStorage = new EncryptStorage(import.meta.env.VITE_STORAGE_ENC_KEY ?? 'd3fau17k37', {
                    prefix: '@' + this.identifier,
                    storageType: storageType === BrowserStorageType.PERSISTENT ? BrowserStorageType.PERSISTENT : BrowserStorageType.SESSION,
                });
            }
        }
    }

    public getStorageType(): BrowserStorageType {
        return this.storageType;
    }

    public getIdentifier(): string {
        return this.identifier;
    }

    public get = (key: string): any | undefined => {
        if (BrowserStorage.defaultStorageType === BrowserStorageType.MEMORY) {
            if (key in Object.keys(this.fakeMemoryStorage)) {
                return this.fakeMemoryStorage[key];
            }
        } else {
            return this.encryptStorage.getItem(key);
        }
        return undefined;
    };

    public set = (key: string, value: any): void => {
        if (BrowserStorage.defaultStorageType === BrowserStorageType.MEMORY) {
            this.fakeMemoryStorage[key] = value;
        } else {
            this.encryptStorage.setItem(key, value);
        }
    };

    public del = (key: string): void => {
        if (BrowserStorage.defaultStorageType === BrowserStorageType.MEMORY) {
            delete this.fakeMemoryStorage[key];
        } else {
            this.encryptStorage.removeItem(key);
        }
    };

    public clearDeleteStorage() {
        /*
            this.encryptStorage.clear(); clears the complete storage of the selected type and does not difference by identifier.
            So we use this custom implementation to clear by identifier prefix.
         */
        if (this.storageType !== BrowserStorageType.MEMORY) {
            const selectedStorage = this.storageType === BrowserStorageType.SESSION ? sessionStorage : localStorage;
            Object.keys(selectedStorage)
                .filter(x => x.startsWith('@' + this.identifier))
                .forEach(x => selectedStorage.removeItem(x));
        }

        for (let i = 0; i < BrowserStorage.instances.length; i++) {
            if (BrowserStorage.instances[i].identifier === this.identifier) {
                BrowserStorage.instances.splice(i, 1);
                break;
            }
        }
    }
}
