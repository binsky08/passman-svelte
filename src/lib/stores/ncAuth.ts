import { writable } from 'svelte/store';
import type {
    NextcloudServerInfoInterface
} from "@binsky/passman-client-ts/lib/Interfaces/NextcloudServer/NextcloudServerInfoInterface";

export const PERSISTENT_AUTH_STORE_ACCESS_KEY: string = 'nc_auth_info';

const ncAuthStore = writable<NextcloudServerInfoInterface>({
    baseUrl: "",
    user: "",
    token: "",	// delete token if login failed at any time, required by AuthGuard
    persistence: "memory"
});

export default ncAuthStore;
