import { writable } from 'svelte/store';
import type { PassmanClient } from "@binsky/passman-client-ts";

const passmanStore = writable<PassmanClient>(undefined);

export default passmanStore;
