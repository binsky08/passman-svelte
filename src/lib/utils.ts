import { notyError, notyInfo } from "./NotyService";
import { BrowserStorage, BrowserStorageType } from "./stores/browser";
import type Vault from "@binsky/passman-client-ts/lib/Model/Vault";
import Credential from "@binsky/passman-client-ts/lib/Model/Credential";

export const debounce = (func, timeout = 300) => {
    let timer;
    return (...args) => {
        clearTimeout(timer);
        timer = setTimeout(() => {
            func.apply(this, args);
        }, timeout);
    };
};

export const copyToClipboard = (value: string, fieldTitle: string) => {
    navigator.clipboard.writeText(value).then(() => {
        /* Resolved - text copied to clipboard successfully */
        //console.log(fieldTitle + ' copied to clipboard');
        notyInfo(fieldTitle + ' copied to clipboard');
    }, () => {
        /* Rejected - text failed to copy to the clipboard */
        //console.error('Failed to copy ' + fieldTitle);
        notyError('Failed to copy ' + fieldTitle);
    });
}

export enum CREDENTIAL_EDIT_SECTIONS {
    GENERAL,
    PASSWORD,
    FILES,
    CUSTOM_FIELDS,
    OTP
}

export enum VAULT_SETTINGS_SECTIONS {
    GENERAL,
    IMPORT,
    EXPORT,
    PASSWORD_AUDIT,
    PASSWORD_SETTINGS,
    SHARING
}

/**
 * Returns true if a valid vault key was found.
 */
export const openVault = (vault: Vault): boolean => {
    // assume a correct vault.vaultKey if it is set
    if (!vault.vaultKey || vault.vaultKey === '') {
        //notyError($_('vault.errors.decryption_key_not_found'));
        // try to find vault key in a browser storage option
        if (BrowserStorage.hasInstance('vault-' + vault.guid)) {
            const vs = BrowserStorage.getInstance('vault-' + vault.guid);
            if (!vault.testVaultKey(vs.get('vaultKey'))) {
                return false;
            }
            // valid vault key detected
            vault.vaultKey = vs.get('vaultKey');
        } else {
            // we have to test all available storage options
            const vs = BrowserStorage.getInstance('vault-' + vault.guid, BrowserStorageType.SESSION);

            // test key from session storage
            if (!vault.testVaultKey(vs.get('vaultKey'))) {
                // test key from persistent storage
                vs.updateStorageType(BrowserStorageType.PERSISTENT);
                if (!vault.testVaultKey(vs.get('vaultKey'))) {
                    vs.clearDeleteStorage();
                    return false;
                } else {
                    // valid vault key detected
                    vault.vaultKey = vs.get('vaultKey');
                }
            } else {
                // valid vault key detected
                vault.vaultKey = vs.get('vaultKey');
            }
        }
    }
    return true;
}

export const deleteCredential = async (credential: Credential) => {
    if (credential.delete_time <= 0) {
        // mark as deleted
        credential.delete_time = new Date().getTime() / 1000;
        return credential.update();
    } else {
        // delete it from the server
        return credential.destroy();
    }
}

export const recoverCredential = (credential: Credential) => {
    if (credential.delete_time > 0) {
        credential.delete_time = 0;
        return credential.update();
    }
}
