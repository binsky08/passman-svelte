import { DefaultLoggingService } from "@binsky/passman-client-ts/lib/Service/DefaultLoggingService";
import { notyError, notyInfo, notySuccess, notyWarning } from "./NotyService";

export class MyLoggingService extends DefaultLoggingService {
    onInfo(message: string) {
        super.onInfo(message);
        notyInfo(message);
    }

    onSuccess(message: string) {
        super.onSuccess(message);
        notySuccess(message);
    }

    onWarning(message: string) {
        super.onWarning(message);
        notyWarning(message);
    }

    onError(message: string) {
        super.onError(message);
        notyError(message);
    }
}
