/* eslint-disable */

// Components: only Home, Loading and NotFound are statically included in the bundle
import VaultsSelect from './routes/Vaults/VaultsSelect.svelte'
import NotFound from "./lib/partials/NotFound.svelte";
import { SvelteComponent } from "svelte";
import VaultsNew from "./routes/Vaults/VaultsNew.svelte";
import VaultsLogin from "./routes/Vaults/VaultsLogin.svelte";
import CredentialNew from "./routes/Credentials/CredentialNew.svelte";
import VaultSettings from "./routes/Vault/VaultSettings.svelte";
import VaultView from "./routes/Vault/VaultView.svelte";
import CredentialEdit from "./routes/Credentials/CredentialEdit.svelte";

// Export the route definition object
export default {
    // Exact path
    '/': VaultsSelect,
    '/vaults/new': VaultsNew,
    '/vaults/login/:vaultGuid': VaultsLogin,
    '/vault/:vaultGuid/new': CredentialNew,
    '/vault/:vaultGuid/settings': VaultSettings,
    '/vault/:vaultGuid/:credentialGuid?': VaultView,
    '/vault/:vaultGuid/:credentialGuid/edit': CredentialEdit,

    /*
    // Wildcard parameter
    // This matches `/wild/*` (with anything after), but NOT `/wild` (with nothing after)
    // This is dynamically imported too
    '/wild/*': wrap({
        // Note that this is a function that returns the import
        // We're adding an artificial delay of 5 seconds so you can experience the loading even on localhost
        // Note that normally the modules loaded with `import()` are cached, so the delay exists only on the first request.
        // In this case, we're adding a delay every time the component is loaded
        asyncComponent: () => import('./routes/Wild.svelte')
            .then((component) => {
                return new Promise((resolve) => {
                    // Wait 5 seconds before returning
                    setTimeout(() => resolve(component), 5000)
                })
            }),
        // Show the loading component while the component is being downloaded
        loadingComponent: Loading,
        // Pass values for the `params` prop of the loading component
        loadingParams: {
            message: 'Loading the Wild route…'
        }
    }),*/

    // Catch-all, must be last
    '*': NotFound as typeof SvelteComponent,
}
