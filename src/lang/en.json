{
  "home": {
    "topic": "Svelte Localization Tutorial",
    "subtopic": "Internationalization and Localization"
  },
  "header": {
    "home": "Home",
    "contact": "Contact Us",
    "about": "About"
  },
  "sidebar": {
    "buttons": {
      "lock_vault": "Lock vault",
      "refresh_vault": "Refresh vault",
      "new_credential": "New credential",
      "analyze_broken": "Analyze credentials with broken encryption",
      "help": "Help",
      "back": "Back",
      "save": "Save",
      "multi": {
        "recover_selected_credentials": "Recover all selected credentials",
        "delete_selected_credentials": "Delete all selected credentials",
        "destroy_selected_credentials": "Destroy all selected credentials",
        "recover_selected_credentials_success": "Selected credentials successfully recovered",
        "delete_selected_credentials_success": "Selected credentials successfully deleted",
        "destroy_selected_credentials_success": "Selected credentials successfully deleted"
      }
    },
    "filter": {
      "show_all": "Show all",
      "compromised": "Compromised",
      "strength_low": "Bad strength",
      "strength_medium": "Medium strength",
      "strength_good": "Good strength",
      "expired": "Expired",
      "deleted": "Deleted"
    },
    "credential_sections": {
      "general": "General",
      "password": "Password",
      "files": "Files",
      "custom_fields": "Custom fields",
      "otp": "OTP"
    },
    "vault_settings_sections": {
      "general": "General settings",
      "import": "Import credentials",
      "export": "Export credentials",
      "password_audit": "Password audit",
      "password_settings": "Password audit",
      "sharing": "Sharing"
    },
    "vault": {
      "vault": "Vault",
      "selected_credentials": "Selected: {selectedCredentialsNumber} of {filteredCredentialsNumber} shown"
    }
  },
  "nextcloud": {
    "login": {
      "login": "Login",
      "password": "Password",
      "user": "User name",
      "nextcloud_url": "Nextcloud server url",
      "storage_option_legend": "Store nextcloud authentication data",
      "storage_option": {
        "memory": "until page reload (in memory)",
        "session": "until session ends (in browser SessionStorage)",
        "persistent": "forever (in browser LocalStorage)"
      }
    },
    "autologin": {
      "succeeded": "Nextcloud auto login succeeded",
      "failed": "Nextcloud auto login failed"
    }
  },
  "vault": {
    "errors": {
      "get_by_uuid_failed": "Failed to get vault by uuid",
      "no_vault_selected": "No vault selected",
      "decryption_key_not_found": "Vault decryption key not found",
      "wrong_password": "Wrong vault password"
    },
    "login": {
      "enter_password_for": "Enter password for {name}",
      "decrypt_vault_button": "Decrypt vault",
      "storage_option_legend": "Store vault authentication data",
      "storage_lock_notice": "Locking the Vault will always clear the cached password."
    },
    "select": {
      "vaults": "Vaults",
      "new": "Create a new vault",
      "created": "Created",
      "last_access": "Last access",
      "never": "never"
    },
    "new": {
      "name": "New vault name",
      "password": "Password",
      "password_repeat": "Password (repeat)",
      "create": "Create vault"
    },
    "list": {
      "copy_to_clipboard": "Copy to clipboard",
      "copy_username_to_clipboard": "Copy username to clipboard",
      "copy_password_to_clipboard": "Copy password to clipboard",
      "copy_otp_to_clipboard": "Copy OTP to clipboard",
      "show_details": "Show details",
      "edit": "Edit",
      "delete": "Delete",
      "destroy": "Destroy",
      "recover": "Recover",
      "select_all": "Select all",
      "select_all_except_this": "Select all except this"
    },
    "settings": {
      "buttons": {
        "change": "Change",
        "delete": "Yes, delete my precious passwords"
      },
      "general": {
        "delete": "Delete vault",
        "delete_irreversible_hint": "This process is irreversible",
        "delete_confirm_hint": "Delete my precious passwords",
        "rename": "Rename vault",
        "new_vault_name": "New vault name",
        "rename_success": "Vault successfully renamed",
        "change_vault_key": "Change vault key",
        "confirm_password": "Confirm vault password",
        "current_password": "Current vault password",
        "new_password": "New vault password",
        "repeat_new_password": "Repeat new vault password",
        "password_missing": "Password missing",
        "change_password_hint": "Since all data has to be downloaded, re-encrypted and uploaded again when changing the password, this process is error-prone. It is strongly recommended to have an updated backup of your vault before changing the password.",
        "accept_risks": "I understand the risks"
      }
    }
  },
  "credential": {
    "field": {
      "label": "Label",
      "username": "User name",
      "password": "Password",
      "email": "E-Mail",
      "url": "URL",
      "description": "Description",
      "expire_time": "Expires",
      "created": "Created",
      "changed": "Changed",
      "tags": "Tags",
      "otp": "OTP",
      "files": "Files"
    },
    "details": {
      "close": "Close",
      "compromised_notice": "This password is compromised. You can only remove this warning by changing the password.",
      "label_successfully_updated": "Label successfully updated",
      "label_update_failed": "Label update failed"
    },
    "files": {
      "download_perm_error": "Error downloading file, you probably have insufficient permissions"
    },
    "errors": {
      "credential_not_found": "Credential not found",
      "update_error": "An error occurred while updating the credential",
      "new_error": "An error occurred while creating the credential",
      "delete_perm_error": "Error deleting credential '{credentialLabel}', you probably have insufficient permissions"
    },
    "edit": {
      "mark_compromised": "Mark as compromised",
      "password_repeat": "Repeat password",
      "password_match_missing": "Passwords do not match",
      "expire_date": "Expiration date",
      "no_expire_date": "No expiration date set",
      "error_no_label": "Please fill in a label",
      "success": "Credential successfully updated",
      "otp": {
        "secret": "OTP secret",
        "type": "Type",
        "issuer": "Issuer",
        "digits": "Digits",
        "period": "Period",
        "algorithm": "Algorithm",
        "label": "Label",
        "secret_invalid": "Invalid secret",
        "qr_invalid": "Invalid QR code",
        "clear_all": "Clear OTP configuration"
      },
      "files": {
        "upload_succeeded": "File upload succeeded",
        "upload_failed": "File upload failed",
        "deletion_succeeded": "File deletion succeeded",
        "deletion_failed": "File deletion failed",
        "header": {
          "name": "File name",
          "created": "Created at",
          "size": "Size"
        }
      },
      "custom_fields": {
        "error_no_file_selected": "No file selected",
        "types": {
          "text": "Text",
          "password": "Password",
          "file": "File"
        },
        "header": {
          "label": "Field label",
          "value": "Field value",
          "type": "Field type",
          "add": "Add field"
        }
      }
    },
    "new": {
      "success": "Credential successfully created"
    }
  },
  "password_generator": {
    "label": "Password generation settings",
    "length": "Password length",
    "useUppercase": "Use uppercase letters",
    "useLowercase": "Use lowercase letters",
    "useDigits": "Use digits",
    "useSpecialChars": "Use special characters",
    "avoidAmbiguousCharacters": "Avoid ambiguous characters",
    "requireEveryCharType": "Require every character type"
  },
  "settings": {
    "button": "Settings"
  },
  "buttons": {
    "abort": "Abort",
    "sign_out": "Sign out",
    "refresh_vaults": "Refresh vault list"
  }
}
